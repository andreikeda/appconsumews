CREATE TABLE Users(
    id int NOT NULL,
    name varchar(255),
    username varchar(255),
    email varchar(255),
    phone varchar(255),
    website varchar(255),

    PRIMARY KEY (id)
);
