CREATE TABLE Company(
    user_id int NOT NULL,
    bs varchar(255),
    catchPhrase varchar(255),
    name varchar(255),

    FOREIGN KEY (user_id) REFERENCES Users(id)
);
