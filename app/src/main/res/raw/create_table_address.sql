CREATE TABLE Address(
    user_id int NOT NULL,
    street varchar(255),
    suite varchar(255),
    city varchar(255),
    zipcode varchar(255),
    lat varchar(255),
    lng varchar(255),

    FOREIGN KEY (user_id) REFERENCES Users(id)
);
