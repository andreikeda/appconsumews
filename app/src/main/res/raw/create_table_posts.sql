CREATE TABLE Posts(
    id int NOT NULL,
    user_id int NOT NULL,
    title varchar(255),
    body varchar(255),

    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES Users(id)
);
