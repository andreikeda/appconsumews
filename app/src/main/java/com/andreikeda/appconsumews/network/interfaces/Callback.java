package com.andreikeda.appconsumews.network.interfaces;

/**
 * Created by Andre Ikeda on 28/04/2014.\n\n
 * Contact: <b>andreikeda87@gmail.com</b>
 *
 * This interface is used to create a bridge of communication between UI Thread and Threads running on background.
 * Objects can be transferred after the Thread has finished or an error message and http status if execution has
 * failed. If necessary, background threads can communicate their start to UI Thread so it's possible to feedback
 * the user about the task running without freezing the screen.
 */
public interface Callback {
    /**
     * Method can be called right before event starts. For example: onPreExecute() for AsyncTask classes.
     *
     * @param executingClass - Caller class, so application will be able to know what feedback must be shown
     *                       to user.
     */
    void onStart(Class<?> executingClass);

    /**
     * Method can be called right after event finishes with ERROR. For example: onPostExecute() for AsyncTask classes.
     *
     * @param executingClass - Caller class, so application will be able to know what feedback must be shown
     *                       to user.
     * @param errorMessage - String if available on task.
     */
    void onError(Class<?> executingClass, String errorMessage);

    /**
     * Method can be called on task progress changes. For example: doInBackground() for AsyncTask classes.
     *
     * @param progress - Integer task progress changes.
     */
    void onProgress(Integer... progress);

    /**
     * Method can be called right after event finishes with SUCCESS. For example: onPostExecute() for AsyncTask classes.
     *
     * @param executingClass - Caller class, so application will be able to expect as result if AsyncTask.doInBackground() has
     *                       generated a result or simple NULL.
     * @param result - Object instance result. If not necessary, AsyncTask.doInBackground() can return Void to AsyncTask.onPostExecute().
     */
    void onSuccess(Class<?> executingClass, Object result);

    /**
     * Method can be called when a task has been cancelled while still running.
     *
     * @param executingClass - Caller class, so application will be able to know what feedback must be shown to user.
     */
    void onCancel(Class<?> executingClass);
}
