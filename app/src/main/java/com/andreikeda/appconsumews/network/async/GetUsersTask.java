package com.andreikeda.appconsumews.network.async;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.andreikeda.appconsumews.R;
import com.andreikeda.appconsumews.model.UserModel;
import com.andreikeda.appconsumews.network.Request;
import com.andreikeda.appconsumews.network.URL;
import com.andreikeda.appconsumews.network.interfaces.Callback;
import com.andreikeda.appconsumews.storage.DatabaseHelper;
import com.andreikeda.appconsumews.utils.LogUtils;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andre Ikeda on 15/06/16.
 */
public class GetUsersTask extends AsyncTask<Void, Integer, List<UserModel>> implements Request.OnStatusReceived {

    private Callback callback;
    private Request.Status connStatus = Request.Status.Http500;
    private DatabaseHelper db;
    private SQLiteDatabase mLite;

    private String errorMessage = "";

    public GetUsersTask(Callback callback, DatabaseHelper db, SQLiteDatabase mLite) {
        this.callback = callback;
        this.db = db;
        this.mLite = mLite;
    }

    @Override
    public void onReceive(Request.Status status) {
        connStatus = status;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callback != null) {
            callback.onStart(getClass());
        }
    }

    @Override
    protected List<UserModel> doInBackground(Void... params) {
        Request request = new Request();
        List<UserModel> result = new ArrayList<>();
        try {
            JSONArray array = request.get(URL.buildUsersUrl(), this);
            for (int index = 0; index < array.length(); index++) {
                UserModel user = new UserModel();
                user.decode(array.getJSONObject(index));
                result.add(user);
                publishProgress((int) ((index * 100) / array.length()));
            }
            for (int index = 0; index < result.size(); index++) {
                db.insertUser(mLite, result.get(index));
                db.insertAddress(mLite, result.get(index).getAddress(), result.get(index).getId());
                db.insertCompany(mLite, result.get(index).getCompany(), result.get(index).getId());
                publishProgress((int) ((index * 100) / result.size()));
            }
        } catch (Exception e) {
            e.printStackTrace();
            errorMessage = e.getMessage();
        }
        return result;
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        if (callback != null) {
            callback.onProgress(progress);
        }
    }

    @Override
    protected void onPostExecute(List<UserModel> result) {
        switch (connStatus) {
            case Http200:
            case Http201:
                callback.onSuccess(getClass(), result);
                break;
            default:
                callback.onError(getClass(), errorMessage);
        }
    }
}
