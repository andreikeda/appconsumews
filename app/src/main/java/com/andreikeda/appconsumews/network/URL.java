package com.andreikeda.appconsumews.network;

/**
 * Created by Andre Ikeda on 15/06/16.
 */
public class URL {

    private final static String URL_HOSTS = "http://jsonplaceholder.typicode.com";
    private final static String URL_POSTS = URL_HOSTS + "/posts?userId=";
    private final static String URL_USERS = URL_HOSTS + "/users";

    public static String buildPostsUrl(int userId) {
        return URL_POSTS + userId;
    }

    public static String buildUsersUrl() {
        return URL_USERS;
    }

}
