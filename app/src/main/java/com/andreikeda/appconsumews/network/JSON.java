package com.andreikeda.appconsumews.network;

/**
 * Created by Andre Ikeda on 15/06/16.
 */
public class JSON {

    public final static String ADDRESS = "address";
    public final static String BODY = "body";
    public final static String BS = "bs";
    public final static String CATCHPHRASE = "catchPhrase";
    public final static String CITY = "city";
    public final static String COMPANY = "company";
    public final static String EMAIL = "email";
    public final static String GEO = "geo";
    public final static String ID = "id";
    public final static String LAT = "lat";
    public final static String LNG = "lng";
    public final static String NAME = "name";
    public final static String PHONE = "phone";
    public final static String STREET = "street";
    public final static String SUITE = "suite";
    public final static String TITLE = "title";
    public final static String USERNAME = "username";
    public final static String WEBSITE = "website";
    public final static String ZIPCODE = "zipcode";

}
