package com.andreikeda.appconsumews.network;

import com.andreikeda.appconsumews.utils.LogUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Class responsible to connect to Client WS API. Requests can be opened from GET, POST and PUT and returns json objects or
 * null if no return is done from the webservice.
 *
 * @author Andre Ikeda<br>
 * contact me: andreikeda87@gmail.com
 *
 * @see JSONObject
 */
public class Request {

	/**
	 * Enum to declare a request response. It can be from HTTP200 to HTTP503.
	 *
	 * @author Andre Ikeda<br>
     * contact me: andreikeda87@gmail.com
	 *
	 */
    public enum Status {
        None,
        Http200,
        Http201,
        Http400,
        Http401,
        Http403,
        Http404,
        Http500,
        Http501,
        Http503;

        /**
         * Method to return Status enum from http status code returned from request.
         * 
         * @param status - enum that represents http status code.
         * @return Status
         */
        public static Status get(int status) {
            switch (status) {
                case 200:
                    return Http200;
                case 201:
                    return Http201;
                case 400:
                    return Http400;
                case 401:
                    return Http401;
                case 403:
                    return Http403;
                case 404:
                    return Http404;
                case 500:
                    return Http500;
                case 501:
                    return Http501;
                case 503:
                    return Http503;
            }
            return None;
        }
    }

    /**
     * Interface used to set http status code received from http request.
     *
     * @author Andre Ikeda<br>
     * contact me: andreikeda87@gmail.com
     *
     */
    public interface OnStatusReceived {
    	/**
    	 * Method to set http status request.
    	 * 
    	 * @param status - Status http request.
    	 */
        void onReceive(Status status);
    }

    private final int BUFFER_SIZE_8K = 8152;

    /**
     * Method detects if there's internet access. It returns <b>true</b> if there's internet access and <b>false</b> otherwise.
     * 
     * @return Boolean
     * @throws IOException
     * @throws IllegalArgumentException
     */
    public boolean hasConnection() throws IOException, IllegalArgumentException {
        HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
        urlc.setConnectTimeout(1500); 
        urlc.connect();
        return Status.get(urlc.getResponseCode()).equals(Status.Http200) || Status.get(urlc.getResponseCode()).equals(Status.Http201);
    }

    /**
     * Method to make HTTP GET request. It returns a {@link JSONObject} or NULL if webservice has not returned content.
     *
     * @param url - String that contains an url to obtain webservice response.
     * @param listener - OnStatusReceived http status code callback.
     * @return JSONObject
     * @throws IOException
     * @throws JSONException
     * @throws IllegalArgumentException
     * @throws NullPointerException
     */
    public JSONArray get(String url, OnStatusReceived listener) throws IOException, IllegalArgumentException, NullPointerException, JSONException {

        URL domain = new URL(url);
        HttpURLConnection httpCon = (HttpURLConnection) domain.openConnection();
        httpCon.setRequestMethod("GET");

        if (listener != null)
            listener.onReceive(Status.get(httpCon.getResponseCode()));
        LogUtils.debug(getClass(), "status:" + httpCon.getResponseCode());

        InputStream instream;
        switch (httpCon.getResponseCode()) {
            case 200:
            case 201:
                instream = httpCon.getInputStream();
                break;
            default:
                instream = httpCon.getErrorStream();
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(
                instream, "utf-8"), BUFFER_SIZE_8K);
        StringBuilder json = new StringBuilder();
        String line = reader.readLine();
        while (line != null) {
            json.append(line);
            line = reader.readLine();
        }
        reader.close();

        return new JSONArray(json.toString());
    }

}
