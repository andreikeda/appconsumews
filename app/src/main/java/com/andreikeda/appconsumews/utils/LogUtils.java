package com.andreikeda.appconsumews.utils;

import android.util.Log;

/**
 *
 * @author Andre Ikeda<br>
 * contact me: andre.ikeda@99motos.com
 *
 */
public class LogUtils {
	
	public static void debug(Class<?> executingClass, String message) {
		Log.d(executingClass.getSimpleName(), message);
	}
	
	public static void error(Class<?> executingClass, String message) {
		Log.e(executingClass.getSimpleName(), message);
	}
	
	public static void info(Class<?> executingClass, String message) {
		Log.i(executingClass.getSimpleName(), message);
	}
	
	public static void verbose(Class<?> executingClass, String message) {
		Log.v(executingClass.getSimpleName(), message);
	}
	
	public static void warning(Class<?> executingClass, String message) {
		Log.w(executingClass.getSimpleName(), message);
	}

}
