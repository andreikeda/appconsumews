package com.andreikeda.appconsumews.activity;

import android.app.ProgressDialog;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.ListView;

import com.andreikeda.appconsumews.R;
import com.andreikeda.appconsumews.adapter.PostsAdapter;
import com.andreikeda.appconsumews.model.UserModel;
import com.andreikeda.appconsumews.storage.DatabaseHelper;

/**
 * Created by Andre Ikeda on 15/06/16.
 */
public class PostsActivity extends CustomActivity {

    private DatabaseHelper db;
    private SQLiteDatabase mLite;
    private UserModel selectedUser;

    private ListView listPosts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posts);
        setViews();

        selectedUser = (UserModel) getIntent().getSerializableExtra(BUNDLE_USER);
        setTitle(selectedUser.getUsername());

        db = new DatabaseHelper(this, getString(R.string.database_name), null, getResources().getInteger(R.integer.database_version));
        mLite = db.getWritableDatabase();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setContent();
    }

    private void setContent() {
        listPosts.setAdapter(new PostsAdapter(this, db.queryPosts(mLite, selectedUser.getId())));
    }

    private void setViews() {
        listPosts = (ListView) findViewById(R.id.list_posts);
    }
}
