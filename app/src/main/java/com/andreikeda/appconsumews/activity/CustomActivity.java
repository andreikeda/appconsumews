package com.andreikeda.appconsumews.activity;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by Andre Ikeda on 15/06/16.
 */
public abstract class CustomActivity extends AppCompatActivity {

    public final String BUNDLE_USER = "bundle_user";

}
