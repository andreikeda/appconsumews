package com.andreikeda.appconsumews.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.andreikeda.appconsumews.R;
import com.andreikeda.appconsumews.model.PostModel;
import com.andreikeda.appconsumews.model.UserModel;
import com.andreikeda.appconsumews.network.async.GetPostsTask;
import com.andreikeda.appconsumews.network.interfaces.Callback;
import com.andreikeda.appconsumews.storage.DatabaseHelper;

import java.util.List;

/**
 * Created by Andre Ikeda on 15/06/16.
 */
public class DetailsActivity extends CustomActivity {

    private DatabaseHelper db;
    private SQLiteDatabase mLite;
    private List<PostModel> posts;
    private ProgressDialog pgLoading;
    private UserModel selectedUser;

    private Button buttonSeePosts;
    private TextView textviewName;
    private TextView textviewEmail;
    private TextView textviewPhone;
    private TextView textviewWebsite;
    private TextView textviewStreet;
    private TextView textviewSuite;
    private TextView textviewCity;
    private TextView textviewZipcode;
    private TextView textviewCompanyName;
    private TextView textviewBs;
    private TextView textviewCatchPhrase;

    private Callback callback = new Callback() {
        @Override
        public void onStart(Class<?> executingClass) {
            showLoading(getString(R.string.loading_posts));
        }

        @Override
        public void onError(Class<?> executingClass, String errorMessage) {
            hideLoading();
        }

        @Override
        public void onProgress(Integer... progress) {
            if (pgLoading != null) {
                pgLoading.setProgress(progress[0]);
            }
        }

        @Override
        public void onSuccess(Class<?> executingClass, Object result) {
            hideLoading();
            setContent();
        }

        @Override
        public void onCancel(Class<?> executingClass) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        setViews();

        selectedUser = (UserModel) getIntent().getSerializableExtra(BUNDLE_USER);
        setTitle(selectedUser.getUsername());

        db = new DatabaseHelper(this, getString(R.string.database_name), null, getResources().getInteger(R.integer.database_version));
        mLite = db.getWritableDatabase();
        selectedUser.setAddress(db.queryAddress(mLite, selectedUser.getId()));
        selectedUser.setCompany(db.queryCompany(mLite, selectedUser.getId()));

        posts = db.queryPosts(mLite, selectedUser.getId());
        if (posts.isEmpty()) {
            new GetPostsTask(callback, db, mLite).execute(selectedUser.getId());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setContent();
    }

    private void hideLoading() {
        if (pgLoading != null &&
                pgLoading.isShowing()) {
            pgLoading.dismiss();
        }
        pgLoading = null;
    }

    private void setContent() {
        textviewBs.setText(selectedUser.getCompany().getBs());
        textviewCatchPhrase.setText(selectedUser.getCompany().getCatchPhrase());
        textviewCity.setText(selectedUser.getAddress().getCity());
        textviewCompanyName.setText(selectedUser.getCompany().getName());
        textviewEmail.setText(selectedUser.getEmail());
        textviewName.setText(selectedUser.getName());
        textviewPhone.setText(selectedUser.getPhone());
        textviewStreet.setText(selectedUser.getAddress().getStreet());
        textviewSuite.setText(selectedUser.getAddress().getSuite());
        textviewWebsite.setText(selectedUser.getWebsite());
        textviewZipcode.setText(selectedUser.getAddress().getZipcode());
        buttonSeePosts.setText(getString(R.string.button_see_posts, selectedUser.getUsername(), db.queryPosts(mLite, selectedUser.getId()).size()));
    }

    private void setViews() {
        buttonSeePosts = (Button) findViewById(R.id.button_see_posts);
        buttonSeePosts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(DetailsActivity.this, PostsActivity.class);
                it.putExtra(BUNDLE_USER, selectedUser);
                startActivity(it);
            }
        });
        textviewName = (TextView) findViewById(R.id.textview_name);
        textviewEmail = (TextView) findViewById(R.id.textview_email);
        textviewPhone = (TextView) findViewById(R.id.textview_phone);
        textviewWebsite = (TextView) findViewById(R.id.textview_website);
        textviewStreet = (TextView) findViewById(R.id.textview_street);
        textviewSuite = (TextView) findViewById(R.id.textview_suite);
        textviewCity = (TextView) findViewById(R.id.textview_city);
        textviewZipcode = (TextView) findViewById(R.id.textview_zipcode);
        textviewCompanyName = (TextView) findViewById(R.id.textview_company_name);
        textviewBs = (TextView) findViewById(R.id.textview_bs);
        textviewCatchPhrase = (TextView) findViewById(R.id.textview_catchPhrase);
    }

    private void showLoading(String message) {
        if (pgLoading == null) {
            pgLoading = new ProgressDialog(this);
        }
        pgLoading.setMessage(message);
        pgLoading.setIndeterminate(false);
        pgLoading.setMax(100);
        pgLoading.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pgLoading.setCancelable(false);
        pgLoading.show();
    }

}
