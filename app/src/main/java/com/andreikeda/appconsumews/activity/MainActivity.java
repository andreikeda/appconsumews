package com.andreikeda.appconsumews.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.andreikeda.appconsumews.R;
import com.andreikeda.appconsumews.adapter.UsersAdapter;
import com.andreikeda.appconsumews.network.async.GetUsersTask;
import com.andreikeda.appconsumews.network.interfaces.Callback;
import com.andreikeda.appconsumews.storage.DatabaseHelper;
import com.andreikeda.appconsumews.storage.Preferences;

public class MainActivity extends CustomActivity {

    private DatabaseHelper db;
    private SQLiteDatabase mLite;
    private UsersAdapter adapter;
    private ProgressDialog pgLoading;
    private ListView listviewUsers;

    private Callback callback = new Callback() {
        @Override
        public void onStart(Class<?> executingClass) {
            showLoading(getString(R.string.loading_users));
        }

        @Override
        public void onError(Class<?> executingClass, String errorMessage) {
            hideLoading();
        }

        @Override
        public void onProgress(Integer... progress) {
            if (pgLoading != null) {
                pgLoading.setProgress(progress[0]);
            }
        }

        @Override
        public void onSuccess(Class<?> executingClass, Object result) {
            hideLoading();
            setContent();
        }

        @Override
        public void onCancel(Class<?> executingClass) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setViews();

        db = new DatabaseHelper(this, getString(R.string.database_name), null, getResources().getInteger(R.integer.database_version));
        mLite = db.getWritableDatabase();

        Preferences mPref = new Preferences(this);
        if (mPref.getFirstTime()) {
            new GetUsersTask(callback, db, mLite).execute();
            mPref.setFirstTime();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        setContent();
    }

    private void hideLoading() {
        if (pgLoading != null &&
                pgLoading.isShowing()) {
            pgLoading.dismiss();
        }
        pgLoading = null;
    }

    private void setContent() {
        adapter = new UsersAdapter(MainActivity.this, db.queryUsers(mLite));
        listviewUsers.setAdapter(adapter);
        listviewUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent it = new Intent(MainActivity.this, DetailsActivity.class);
                it.putExtra(BUNDLE_USER, adapter.getItem(position));
                startActivity(it);
            }
        });
    }

    private void setViews() {
        listviewUsers = (ListView) findViewById(R.id.list_users);
    }

    private void showLoading(String message) {
        if (pgLoading == null) {
            pgLoading = new ProgressDialog(this);
        }
        pgLoading.setMessage(message);
        pgLoading.setIndeterminate(false);
        pgLoading.setMax(100);
        pgLoading.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pgLoading.setCancelable(false);
        pgLoading.show();
    }
}
