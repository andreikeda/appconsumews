package com.andreikeda.appconsumews.model;

import com.andreikeda.appconsumews.network.JSON;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Andre Ikeda on 15/06/16.
 */
public class AddressModel extends GenericModel {

    private String street;
    private String suite;
    private String city;
    private String zipcode;

    private GeoModel geo;

    public AddressModel() {

    }

    public AddressModel(String street, String suite, String city, String zipcode, String lat, String lng) {
        this.street = street;
        this.suite = suite;
        this.city = city;
        this.zipcode = zipcode;
        this.geo = new GeoModel(lat, lng);
    }

    @Override
    public void decode(JSONObject json) throws JSONException {
        if (json.has(JSON.CITY)) {
            setCity(json.getString(JSON.CITY));
        }
        if (json.has(JSON.STREET)) {
            setStreet(json.getString(JSON.STREET));
        }
        if (json.has(JSON.SUITE)) {
            setSuite(json.getString(JSON.SUITE));
        }
        if (json.has(JSON.ZIPCODE)) {
            setZipcode(json.getString(JSON.ZIPCODE));
        }
        if (json.has(JSON.GEO)) {
            setGeo(json.getJSONObject(JSON.GEO));
        }
    }

    @Override
    public JSONObject encode() throws JSONException {
        JSONObject json = new JSONObject();
        if (city != null) {
            json.put(JSON.CITY, getCity());
        }
        if (geo != null) {
            json.put(JSON.GEO, getGeo().encode());
        }
        if (street != null) {
            json.put(JSON.STREET, getStreet());
        }
        if (suite != null) {
            json.put(JSON.SUITE, getSuite());
        }
        if (zipcode != null) {
            json.put(JSON.ZIPCODE, getZipcode());
        }
        return json;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public GeoModel getGeo() {
        return geo;
    }

    public void setGeo(GeoModel geo) {
        this.geo = geo;
    }

    public void setGeo(JSONObject json) {
        this.geo = new GeoModel();
        try {
            this.geo.decode(json);
        } catch (JSONException e) {
            // Maybe a place doesn\'t have a geoaddress, so, we just decode if there is a geoaddress.
            this.geo.setLng("0.0");
            this.geo.setLat("0.0");
            e.printStackTrace();
        }
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getSuite() {
        return suite;
    }

    public void setSuite(String suite) {
        this.suite = suite;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }
}
