package com.andreikeda.appconsumews.model;

import com.andreikeda.appconsumews.network.JSON;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Andre Ikeda on 15/06/16.
 */
public class PostModel extends GenericModel {

    private int id;
    private String title;
    private String body;

    public PostModel() {

    }

    public PostModel(int id, String title, String body) {
        this.id = id;
        this.title = title;
        this.body = body;
    }

    @Override
    public void decode(JSONObject json) throws JSONException {
        setBody(json.getString(JSON.BODY));
        setId(json.getInt(JSON.ID));
        setTitle(json.getString(JSON.TITLE));
    }

    @Override
    public JSONObject encode() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(JSON.BODY, getBody());
        json.put(JSON.ID, getId());
        json.put(JSON.TITLE, getTitle());
        return json;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
