package com.andreikeda.appconsumews.model;

import com.andreikeda.appconsumews.network.JSON;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Andre Ikeda on 15/06/16.
 */
public class GeoModel extends GenericModel {

    private String lat;
    private String lng;

    public GeoModel() {

    }

    public GeoModel(String lat, String lng) {
        this.lat = lat;
        this.lng = lng;
    }

    @Override
    public void decode(JSONObject json) throws JSONException {
        setLat(json.getString(JSON.LAT));
        setLng(json.getString(JSON.LNG));
    }

    @Override
    public JSONObject encode() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(JSON.LAT, getLat());
        json.put(JSON.LNG, getLng());
        return json;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
