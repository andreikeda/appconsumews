package com.andreikeda.appconsumews.model;

import com.andreikeda.appconsumews.network.JSON;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Andre Ikeda on 15/06/16.
 *
 */
public class UserModel extends GenericModel {

    private int id = -1;
    private String name;
    private String username;
    private String email;
    private String phone;
    private String website;

    private AddressModel address;
    private CompanyModel company;

    public UserModel() {

    }

    public UserModel(int id, String name, String username, String email, String phone, String website) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.website = website;
    }

    @Override
    public void decode(JSONObject json) throws JSONException {
        if (json.has(JSON.NAME)) {
            setName(json.getString(JSON.NAME));
        }
        if (json.has(JSON.ID)) {
            setId(json.getInt(JSON.ID));
        }
        if (json.has(JSON.USERNAME)) {
            setUsername(json.getString(JSON.USERNAME));
        }
        if (json.has(JSON.EMAIL)) {
            setEmail(json.getString(JSON.EMAIL));
        }
        if (json.has(JSON.PHONE)) {
            setPhone(json.getString(JSON.PHONE));
        }
        if (json.has(JSON.WEBSITE)) {
            setWebsite(json.getString(JSON.WEBSITE));
        }
        if (json.has(JSON.ADDRESS)) {
            setAddress(json.getJSONObject(JSON.ADDRESS));
        }
        if (json.has(JSON.COMPANY)) {
            setCompany(json.getJSONObject(JSON.COMPANY));
        }
    }

    @Override
    public JSONObject encode() throws JSONException {
        JSONObject json = new JSONObject();
        if (address != null) {
            json.put(JSON.ADDRESS, getAddress());
        }
        if (company != null) {
            json.put(JSON.COMPANY, getCompany());
        }
        if (email != null) {
            json.put(JSON.EMAIL, getEmail());
        }
        if (id != -1) {
            json.put(JSON.ID, getId());
        }
        if (name != null) {
            json.put(JSON.NAME, getName());
        }
        if (phone != null) {
            json.put(JSON.PHONE, getPhone());
        }
        if (username != null) {
            json.put(JSON.USERNAME, getUsername());
        }
        if (website != null) {
            json.put(JSON.WEBSITE, getWebsite());
        }
        return json;
    }

    public AddressModel getAddress() {
        return address;
    }

    public void setAddress(AddressModel address) {
        this.address = address;
    }

    public void setAddress(JSONObject json) {
        this.address = new AddressModel();
        try {
            this.address.decode(json);
        } catch (JSONException e) {

        }
    }

    public CompanyModel getCompany() {
        return company;
    }

    public void setCompany(CompanyModel company) {
        this.company = company;
    }

    public void setCompany(JSONObject json) {
        this.company = new CompanyModel();
        try {
            this.company.decode(json);
        } catch (JSONException e) {

        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
}
