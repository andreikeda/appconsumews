package com.andreikeda.appconsumews.model;

import com.andreikeda.appconsumews.network.JSON;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Andre Ikeda on 15/06/16.
 *
 */
public class CompanyModel extends GenericModel {

    private String bs;
    private String catchPhrase;
    private String name;

    public CompanyModel() {

    }

    public CompanyModel(String bs, String catchPhrase, String name) {
        this.bs = bs;
        this.catchPhrase = catchPhrase;
        this.name = name;
    }

    @Override
    public void decode(JSONObject json) throws JSONException {
        if (json.has(JSON.BS)) {
            setBs(json.getString(JSON.BS));
        }
        if (json.has(JSON.CATCHPHRASE)) {
            setCatchPhrase(json.getString(JSON.CATCHPHRASE));
        }
        if (json.has(JSON.NAME)) {
            setName(json.getString(JSON.NAME));
        }
    }

    @Override
    public JSONObject encode() throws JSONException {
        JSONObject json = new JSONObject();
        if (bs != null) {
            json.put(JSON.BS, getBs());
        }
        if (catchPhrase != null) {
            json.put(JSON.CATCHPHRASE, getCatchPhrase());
        }
        if (name != null) {
            json.put(JSON.NAME, getName());
        }
        return json;
    }

    public String getBs() {
        return bs;
    }

    public void setBs(String bs) {
        this.bs = bs;
    }

    public String getCatchPhrase() {
        return catchPhrase;
    }

    public void setCatchPhrase(String catchPhrase) {
        this.catchPhrase = catchPhrase;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
