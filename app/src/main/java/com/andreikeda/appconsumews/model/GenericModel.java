package com.andreikeda.appconsumews.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Andre Ikeda on 15/06/16.
 */
public abstract class GenericModel implements Serializable {

    public abstract void decode(JSONObject json) throws JSONException;

    public abstract JSONObject encode() throws JSONException;

}
