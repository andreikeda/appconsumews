package com.andreikeda.appconsumews.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.andreikeda.appconsumews.R;
import com.andreikeda.appconsumews.model.PostModel;
import com.andreikeda.appconsumews.model.UserModel;

import java.util.List;

/**
 * Created by Andre Ikeda on 15/06/16.
 */
public class PostsAdapter extends BaseAdapter {

    private Context context;
    private List<PostModel> posts;

    public PostsAdapter(Context context, List<PostModel> posts) {
        this.context = context;
        this.posts = posts;
    }

    @Override
    public int getCount() {
        return posts.size();
    }

    @Override
    public PostModel getItem(int position) {
        return posts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return posts.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_post, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.textviewTitle.setText(getItem(position).getTitle());
        holder.textviewBody.setText(getItem(position).getBody());
        return convertView;
    }

    class ViewHolder {
        protected TextView textviewTitle;
        protected TextView textviewBody;

        public ViewHolder(View view) {
            textviewTitle = (TextView) view.findViewById(R.id.textview_title);
            textviewBody = (TextView) view.findViewById(R.id.textview_body);
        }
    }
}
