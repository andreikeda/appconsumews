package com.andreikeda.appconsumews.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.andreikeda.appconsumews.R;
import com.andreikeda.appconsumews.model.UserModel;

import java.util.List;

/**
 * Created by Andre Ikeda on 15/06/16.
 */
public class UsersAdapter extends BaseAdapter {

    private Context context;
    private List<UserModel> users;

    public UsersAdapter(Context context, List<UserModel> users) {
        this.context = context;
        this.users = users;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public UserModel getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return users.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_users, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.textviewNickname.setText(getItem(position).getUsername());
        holder.textviewName.setText(getItem(position).getName());
        holder.textviewEmail.setText(getItem(position).getEmail());
        return convertView;
    }

    class ViewHolder {
        protected TextView textviewNickname;
        protected TextView textviewName;
        protected TextView textviewEmail;

        public ViewHolder(View view) {
            textviewEmail = (TextView) view.findViewById(R.id.textview_email);
            textviewName = (TextView) view.findViewById(R.id.textview_name);
            textviewNickname = (TextView) view.findViewById(R.id.textview_nickname);
        }
    }
}
