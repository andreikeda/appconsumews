package com.andreikeda.appconsumews.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.andreikeda.appconsumews.R;

/**
 * Created by Andre Ikeda on 15/06/16.
 */
public class Preferences {

    private final String PREF_FIRST_TIME = "first_time";

    private SharedPreferences mPref;

    public Preferences(Context context) {
        mPref = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    public boolean getFirstTime() {
        return mPref.getBoolean(PREF_FIRST_TIME, true);
    }

    public void setFirstTime() {
        mPref.edit()
                .putBoolean(PREF_FIRST_TIME, false)
                .apply();
    }

}
