package com.andreikeda.appconsumews.storage;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.andreikeda.appconsumews.R;
import com.andreikeda.appconsumews.model.AddressModel;
import com.andreikeda.appconsumews.model.CompanyModel;
import com.andreikeda.appconsumews.model.PostModel;
import com.andreikeda.appconsumews.model.UserModel;
import com.andreikeda.appconsumews.network.JSON;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andre Ikeda on 15/06/16.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private final String TABLE_ADDRESS = "Address";
    private final String TABLE_COMPANY = "Company";
    private final String TABLE_POSTS = "Posts";
    private final String TABLE_USERS = "Users";

    private String SQL_INSERT_ADDRESS = "INSERT INTO Address VALUES(%d, '%s', '%s', '%s', '%s', '%s', '%s');";
    private String SQL_INSERT_COMPANY = "INSERT INTO Company VALUES(%d, '%s', '%s', '%s');";
    private String SQL_INSERT_POSTS = "INSERT INTO Posts VALUES(%d, %d, '%s', '%s');";
    private String SQL_INSERT_USER = "INSERT INTO Users VALUES(%d, '%s', '%s', '%s', '%s', '%s');";

    private Context context;

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        setContext(context);
    }

    public void insertAddress(SQLiteDatabase db, AddressModel address, int userId) {
        db.execSQL(
                String.format(
                        SQL_INSERT_ADDRESS,
                        userId,
                        address.getStreet(),
                        address.getSuite(),
                        address.getCity(),
                        address.getZipcode(),
                        address.getGeo().getLat(),
                        address.getGeo().getLng()
                )
        );
    }

    public void insertCompany(SQLiteDatabase db, CompanyModel company, int userId) {
        db.execSQL(
                String.format(
                        SQL_INSERT_COMPANY,
                        userId,
                        company.getBs(),
                        company.getCatchPhrase(),
                        company.getName()
                )
        );
    }

    public void insertPosts(SQLiteDatabase db, PostModel post, int userId) {
        db.execSQL(
                String.format(
                        SQL_INSERT_POSTS,
                        post.getId(),
                        userId,
                        post.getTitle(),
                        post.getBody()
                )
        );
    }

    public void insertUser(SQLiteDatabase db, UserModel user) {
        db.execSQL(
                String.format(
                        SQL_INSERT_USER,
                        user.getId(),
                        user.getName(),
                        user.getUsername(),
                        user.getEmail(),
                        user.getPhone(),
                        user.getWebsite()
                )
        );
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(FileReader.readRawTextFile(getContext(), R.raw.create_table_users));
        db.execSQL(FileReader.readRawTextFile(getContext(), R.raw.create_table_address));
        db.execSQL(FileReader.readRawTextFile(getContext(), R.raw.create_table_company));
        db.execSQL(FileReader.readRawTextFile(getContext(), R.raw.create_table_posts));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public AddressModel queryAddress(SQLiteDatabase db, int userId) {
        Cursor c = db.query(TABLE_ADDRESS, null, "user_id = ?", new String[]{String.valueOf(userId)}, null, null, null);
        if (c.moveToFirst()) {
            AddressModel address = new AddressModel(
                    c.getString(c.getColumnIndex(JSON.STREET)),
                    c.getString(c.getColumnIndex(JSON.SUITE)),
                    c.getString(c.getColumnIndex(JSON.CITY)),
                    c.getString(c.getColumnIndex(JSON.ZIPCODE)),
                    c.getString(c.getColumnIndex(JSON.LAT)),
                    c.getString(c.getColumnIndex(JSON.LNG))
            );
            return address;
        }
        return null;
    }

    public CompanyModel queryCompany(SQLiteDatabase db, int userId) {
        Cursor c = db.query(TABLE_COMPANY, null, "user_id = ?", new String[]{String.valueOf(userId)}, null, null, null);
        if (c.moveToFirst()) {
            CompanyModel company = new CompanyModel(
                    c.getString(c.getColumnIndex(JSON.BS)),
                    c.getString(c.getColumnIndex(JSON.CATCHPHRASE)),
                    c.getString(c.getColumnIndex(JSON.NAME))
            );
            return company;
        }
        return null;
    }

    public List<PostModel> queryPosts(SQLiteDatabase db, int userId) {
        Cursor c = db.query(TABLE_POSTS, null, "user_id = ?", new String[]{String.valueOf(userId)}, null, null, null);
        List<PostModel> posts = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                PostModel post = new PostModel(
                        c.getInt(c.getColumnIndex(JSON.ID)),
                        c.getString(c.getColumnIndex(JSON.TITLE)),
                        c.getString(c.getColumnIndex(JSON.BODY))
                );
                posts.add(post);
            } while (c.moveToNext());
        }
        return posts;
    }

    public List<UserModel> queryUsers(SQLiteDatabase db) {
        Cursor c = db.query(TABLE_USERS, null, null, null, null, null, null, null);
        List<UserModel> users = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                UserModel user = new UserModel(
                        c.getInt(c.getColumnIndex(JSON.ID)),
                        c.getString(c.getColumnIndex(JSON.NAME)),
                        c.getString(c.getColumnIndex(JSON.USERNAME)),
                        c.getString(c.getColumnIndex(JSON.EMAIL)),
                        c.getString(c.getColumnIndex(JSON.PHONE)),
                        c.getString(c.getColumnIndex(JSON.WEBSITE))
                );
                users.add(user);
            } while (c.moveToNext());
        }
        return users;
    }

    private Context getContext() {
        return context;
    }

    private void setContext(Context context) {
        this.context = context;
    }
}
